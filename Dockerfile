# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or

FROM python:3.8-alpine
LABEL maintainer="pipinfitriadi@gmail.com"

COPY satupeta-api/requirements.txt /tmp/

RUN apk add --no-cache --virtual .build-deps \
    && pip install --no-cache-dir -r /tmp/requirements.txt \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | sort -u \
            | xargs -r apk info --installed \
            | sort -u \
        )" \
    && apk add --virtual .rundeps $runDeps \
    && apk del --no-cache .build-deps

COPY . /root
WORKDIR /root
RUN rm satupeta-api/requirements.txt

ARG DOCKER_WEB_HOST=0.0.0.0
ARG DOCKER_WEB_PORT=5000
ENV DOCKER_WEB_HOST=$DOCKER_WEB_HOST \
    DOCKER_WEB_PORT=$DOCKER_WEB_PORT

EXPOSE $DOCKER_WEB_PORT

# Gunicorn v 19 gives timeout for Flask http get request which taking time to execute:
# https://github.com/benoitc/gunicorn/issues/2020
CMD ["sh", "-c", "gunicorn -w 4 -t 400 -b $DOCKER_WEB_HOST:$DOCKER_WEB_PORT satupeta-api.wsgi:app"]
