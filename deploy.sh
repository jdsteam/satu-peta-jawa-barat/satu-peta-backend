#!/bin/bash

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or

set -e
# Docker Registry login and docker CI template
# https://gitlab.com/gitlab-org/gitlab-runner/issues/2861
echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
docker-compose -f ~/scripts/$CI_PROJECT_NAME/docker-compose.yml stop
docker pull $CONTAINER_RELEASE_IMAGE
docker-compose -f ~/scripts/$CI_PROJECT_NAME/docker-compose.yml up -d
# How to ignore xargs commands if stdin input is empty
# https://stackoverflow.com/questions/8296710/how-to-ignore-xargs-commands-if-stdin-input-is-empty
docker images -q -f dangling=true -f reference=$CI_REGISTRY_IMAGE -f reference=python | xargs -r docker rmi
