#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import os

from flask import Blueprint, redirect, send_from_directory, url_for

from .library import response_json

app = Blueprint('route', __name__, url_prefix='/')


@app.route('/doc/')
@app.route('/documentation/')
def root():
    return redirect(
        url_for('doc.show')
    )


@app.route('/license/')
def license():
    return send_from_directory(
        os.path.join(app.root_path, '..'),
        'LICENSE',
        mimetype='text/plain'
    )


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(
        'static',
        'favicon.ico',
        mimetype='image/vnd.microsoft.icon'
    )


@app.route('/sektoral/')
def sektoral():
    with open(
        os.path.join(
            app.root_path,
            'static',
            'sektoral.json'
        ),
        'r'
    ) as f:
        data = f.read()

    return response_json(
        json.loads(data)
    )


@app.route('/opd/')
def opd():
    with open(
        os.path.join(
            app.root_path,
            'static',
            'opd.json'
        ),
        'r'
    ) as f:
        data = f.read()

    return response_json(
        json.loads(data)
    )
