#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or

from os import getenv

from datetime import timedelta
from dotenv import load_dotenv


class Config(object):
    load_dotenv()

    # Database
    SQLALCHEMY_DATABASE_URI = (
        f'postgresql://{ getenv("DB_USER", "user") }:'
        + f'{ getenv("DB_PASS", "pass") }@{ getenv("DB_HOST", "localhost") }:'
        + f'{ getenv("DB_PORT", "5432") }/{ getenv("DB_NAME", "name") }'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT
    JWT_ALGORITHM = getenv('JWT_ALGORITHM', 'algorithm')
    JWT_SECRET_KEY = getenv('JWT_SECRET_KEY', 'secret')
    JWT_AUTH_URL_RULE = None
    # Token expired in 1 weeks
    JWT_EXPIRATION_DELTA = timedelta(days=7)
