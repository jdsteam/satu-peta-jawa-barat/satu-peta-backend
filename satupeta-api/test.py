#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Unit Testing a Flask Application
# https://www.patricksoftwareblog.com/unit-testing-a-flask-application/

from unittest import main, TestCase

from .wsgi import app


class BasicTest(TestCase):
    def setUp(self):
        '''Executed prior to each test.'''
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        '''Executed after each test.'''
        pass

    def check_response(
        self,
        route,
        mode='status_code',
        expected=200
    ):
        response = self.app.head(route)

        if mode == 'mimetype':
            result = response.mimetype
        else:
            result = response.status_code

        self.assertEqual(
            result,
            expected
        )
        # helpers.send_file doesn't close file
        # https://github.com/pallets/flask/issues/2468
        response.close()

    def test_license(self):
        self.check_response(
            '/license/',
            'mimetype',
            'text/plain'
        )

    def test_favicon(self):
        self.check_response(
            '/favicon.ico',
            'mimetype',
            'image/vnd.microsoft.icon'
        )

    def test_unknown_path(self):
        self.check_response('/unknown_path/', expected=404)

    def test_doc(self):
        self.check_response('/')

    def test_sektoral(self):
        self.check_response('/sektoral/')

    def test_opd(self):
        self.check_response('/opd/')


if __name__ == '__main__':
    main()
