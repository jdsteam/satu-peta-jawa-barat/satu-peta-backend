#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import Flask
from flask_cors import CORS
from flask_compress import Compress
from flask_jwt_extended import JWTManager
from flask_socketio import SocketIO
from flask_sqlalchemy import SQLAlchemy
from flask_swagger_ui import get_swaggerui_blueprint

from . import config, error, route


def create_app(debug=False):
    app = Flask(__name__)
    app.debug = debug
    app.config.from_object(config.Config)

    # Register bueprint's route
    app.register_blueprint(
        get_swaggerui_blueprint(
            '',
            # Pergunakan http://editor.swagger.io/ untuk validasi json
            '/static/swagger.json',
            blueprint_name='doc',
            config={
                'app_name': 'SatuPeta-API'
            }
        ),
        url_prefix='/'
    )
    app.register_blueprint(error.app)
    app.register_blueprint(route.app)

    # Init app
    CORS().init_app(app, support_credentials=True)
    Compress().init_app(app)
    SocketIO(
        async_mode='threading',
        ping_timeout=7200
    ).init_app(app)
    JWTManager().init_app(app)
    SQLAlchemy().init_app(app)

    return app
