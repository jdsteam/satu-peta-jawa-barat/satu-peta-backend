# SatuPeta-API

_RESTful API_ Satu Peta.

<!-- TOC -->

- [SatuPeta-API](#satupeta-api)
    - [Requirements](#requirements)
    - [Usefull Command](#usefull-command)
    - [Source-code Editor](#source-code-editor)
        - [Extension](#extension)
        - [Pengaturan](#pengaturan)
            - [`{{root}}/.markdownlint.json`](#rootmarkdownlintjson)
            - [`{{root}}/.vscode/settings.json`](#rootvscodesettingsjson)
            - [`{{root}}/.vscode/launch.json`](#rootvscodelaunchjson)
    - [Referensi](#referensi)
    - [Lisensi](#lisensi)

<!-- /TOC -->

---

## _Requirements_

- Pastikan [_GitLab CI/CD's variable_](https://gitlab.com/jdsteam/satu-peta-jawa-barat/satu-peta-backend/-/settings/ci_cd)
dan `{{root}}/.env` _file_ terisi, untuk kelancaran proses _GitLab CI/CD_. Adapun
rincian pengisiannya adalah sebagai berikut:

    ```conf
    # Server
    DEPLOYMENT_SERVER_IP={{DEPLOYMENT_SERVER_IP}}
    DEPLOYMENT_SERVER_PASS={{DEPLOYMENT_SERVER_PASS}}
    DEPLOYMENT_SERVER_USER={{DEPLOYMENT_SERVER_USER}}

    # Web
    DOCKER_WEB_HOST=0.0.0.0
    DOCKER_WEB_PORT=80
    WEB_PORT={{WEB_PORT}}

    # Database
    DB_NAME={{DB_NAME}}
    DB_HOST={{DB_HOST}}
    DB_USER={{DB_USER}}
    DB_PASS={{DB_PASS}}

    # JWT
    JWT_ALGORITHM={{JWT_ALGORITHM}}
    JWT_SECRET_KEY={{JWT_SECRET_KEY}}
    ```

- [Python 3](https://www.python.org/downloads/) dipergunakan untuk menjalankan SatuPeta-API.
- Pastikan [_library_](satupeta-api/requirements.txt) Python telah ter-_install_.
- _GitLab CI/CD_ dipergunakan dalam proses _deployment_ ke _server_ dengan
_Specific Runner_ dan _executor docker_.

    ```shell
    sudo gitlab-runner register -n \
        --url https://gitlab.com/ \
        --registration-token {{REGISTRATION_TOKEN}} \
        --executor docker \
        --description "server_satupeta_api_runner" \
        --docker-image "docker:19.03.1" \
        --docker-privileged \
        --docker-volumes "/certs/client" \
        --docker-wait-for-services-timeout 400 \
        --tag-list live
    ```

- Ada penyesuaian pada file `/etc/nginx/nginx.conf`

    ```conf
    ...
    events {
        ...
        multi_accept on;
    }

    http {
        ...
        keepalive_timeout 400;
        ...
        server_tokens off;

        server_names_hash_bucket_size 64;
        ...
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 5;
        ...
        gzip_http_version 1.1;
        gzip_min_length 256;
        gzip_types
            application/atom+xml
            application/javascript
            application/json
            application/rss+xml
            application/vnd.ms-fontobject
            application/x-font-ttf
            application/x-web-app-manifest+json
            application/xhtml+xml
            application/xml
            font/opentype
            image/svg+xml
            image/x-icon
            text/css
            text/plain
            text/x-component
            text/javascript
            text/xml;
        ...
    ```

- Ada penyesuaian pada file `/etc/nginx/sites-available/default`

    ```conf
    ...
    server {
        listen 80;
        server_name satupeta-api.103.122.5.84.xip.io;

        location / {
            proxy_set_header Host $host;
            proxy_pass http://0.0.0.0:{{WEB_PORT}};
            proxy_redirect off;
            proxy_connect_timeout 400;
            proxy_read_timeout 400;
        }
    }
    ...
    ```

---

## _Usefull Command_

- [Menyelesaikan masalah _ssh “permissions are too open” error_](https://stackoverflow.com/questions/9270734/ssh-permissions-are-too-open-error):

    ```shell
    $ chmod 400 {{ssh_pkey_file_path}}
    ```

- [Masuk ke _production server_](https://serverpilot.io/docs/how-to-use-ssh-public-key-authentication):

    ```shell
    $ ssh -i {{ssh_pkey_file_path}} {{ssh_username}}@{{ssh_host}}
    ```

- Membuat _image docker_:

    ```shell
    $ docker build -t satupeta-api .
    ```

- Melihat daftar _container docker_ yang sedang dipergunakan:

    ```shell
    $ docker ps
    ```

- Menjalankan _container docker_:
    - Flask _app_:

        ```shell
        $ docker run -dp {{WEB_PORT}}:{DOCKER_WEB_PORT}} --rm {{image_id}}
        ```

    - _Image docker_ secara umum:

        ```shell
        $ docker run -it --rm {{image_id}}
        ```

    - [_Starting a shell in the Docker Alpine container_](https://stackoverflow.com/questions/35689628/starting-a-shell-in-the-docker-alpine-container):

        ```shell
        $ docker run -it --rm {{image_id}} /bin/ash
        ```

- [Masuk ke dalam _container docker_ yang sedang berjalan](https://stackoverflow.com/questions/20932357/how-to-enter-in-a-docker-container-already-running-with-a-new-tty):

    ```shell
    $ docker exec -it {{container_id}} /bin/ash
    ```

- Mengakhiri jalannya _container docker_:

    ```shell
    $ docker stop {{container_id}}
    ```

- Melihat daftar _image docker_ yang sedang dipergunakan:

    ```shell
    $ docker images
    ```

- Menghapus _image docker_:

    ```shell
    $ docker rmi -f {{image_id}}
    ```

---

## _Source-code Editor_

[VSCode](https://code.visualstudio.com/) (Visual Studio Code) dipergunakan untuk
memudahkan dalam penulisan _source-code_.

### _Extension_

Beberapa _extension_ juga perlu dipasangkan ke VSCode:

- [_Auto Markdown TOC_](https://marketplace.visualstudio.com/items?itemName=huntertran.auto-markdown-toc)
- [_Markdownlint_](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- ~~[_Markdown Preview with Bitbucket Styles_](https://marketplace.visualstudio.com/items?itemName=hbrok.markdown-preview-bitbucket)~~
- [_Python extension for Visual Studio Code_](https://marketplace.visualstudio.com/items?itemName=ms-python.python)

### Pengaturan

#### `{{root}}/.markdownlint.json`

```json
{
    "MD007": false,
    "MD013": {
        "code_blocks": false
    },
    "MD014": false
}
```

Aturan penggunaannya dapat dilihat [di sini](https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md).

#### `{{root}}/.vscode/settings.json`

```json
{
    "markdown-toc.updateOnSave": false,
    "python.analysis.disabled": ["undefined-variable", "unresolved-import"],
    "python.linting.flake8Enabled": true,
    "python.linting.flake8Args": ["--ignore=E203,E231,W503"],
    "python.pythonPath": "${workspaceFolder}/env/bin/python",
    "files.exclude": {
        "**/.classpath": true,
        "**/.project": true,
        "**/.settings": true,
        "**/.factorypath": true
    },
}
```

#### `{{root}}/.vscode/launch.json`

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Current File",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal"
        },
        {
            "name": "Python: Flask",
            "type": "python",
            "request": "launch",
            "module": "flask",
            "env": {
                "FLASK_APP": "satupeta-api",
                "FLASK_ENV": "development",
                "FLASK_DEBUG": "0"
            },
            "args": [
                "run",
                "--no-debugger",
                "--no-reload",
                "--port",
                "80",
                "--host",
                "0.0.0.0"
            ],
            "jinja": true
        }
    ]
}
```

---

## Referensi

- [_Deploying a scalable Flask app using Gunicorn and Nginx, in Docker: Part #1_](https://medium.com/@kmmanoj/deploying-a-scalable-flask-app-using-gunicorn-and-nginx-in-docker-part-1-3344f13c9649)
- [_Export secret file to Gitlab pipeline_](https://medium.com/@michalkalita/export-secret-file-to-gitlab-pipeline-75789eee35bd)
- [_Deploying WSGI Python applications using Docker_](https://cobuildlab.com/development-blog/deploying-python-wsgi-application-using-Docker/)
- [_A scalable Flask application using Gunicorn on Ubuntu 18.04 in Docker_](https://philchen.com/2019/07/09/a-scalable-flask-application-using-gunicorn-on-ubuntu-18-04-in-docker)
- [_Docker remove none TAG images_](https://stackoverflow.com/questions/33913020/docker-remove-none-tag-images)
- [_Deploying Flask Apps Easily with Docker and Nginx_](https://ianlondon.github.io/blog/deploy-flask-docker-nginx/)
- [_The Flask Mega-Tutorial Part XIX: Deployment on Docker Containers_](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xix-deployment-on-docker-containers)
- [_Building Minimal Docker Containers for Python Applications_](https://blog.realkinetic.com/building-minimal-docker-containers-for-python-applications-37d0272c52f3)
- [_Benchmarking Debian vs Alpine as a Base Docker Image_](https://nickjanetakis.com/blog/benchmarking-debian-vs-alpine-as-a-base-docker-image)
- [_Deploying a python-django application using docker_](https://dev.to/lewiskori/deploying-a-python-django-application-using-docker-3d09)
- [_How to use Django, PostgreSQL, and Docker_](https://wsvincent.com/django-docker-postgresql/)
- [_Here's a Production-Ready Dockerfile for Your Python/Django App_](https://www.caktusgroup.com/blog/2017/03/14/production-ready-dockerfile-your-python-django-app/)
- [_Hello World - Python_](https://knative.dev/docs/serving/samples/hello-world/helloworld-python/)
- [_Slimming Down Your Docker Images_](https://towardsdatascience.com/slimming-down-your-docker-images-275f0ca9337e)
- [_Alpine Based Docker Images Make a Difference in Real World Apps_](https://blog.codeship.com/alpine-based-docker-images-make-difference-real-world-apps/)
- [_Setting Up GitLab CI for a Python Application_](http://www.patricksoftwareblog.com/setting-up-gitlab-ci-for-a-python-application/)
- [_Building Django Docker Image with Alpine_](https://medium.com/c0d1um/building-django-docker-image-with-alpine-32de65d2706)
- [_Cannot “pip install cryptography” in Docker Alpine Linux 3.3 with OpenSSL
1.0.2g and Python 2.7_](https://stackoverflow.com/questions/35736598/cannot-pip-install-cryptography-in-docker-alpine-linux-3-3-with-openssl-1-0-2g)
- [_Reduce Docker image sizes using Alpine_](https://www.sandtable.com/reduce-docker-image-sizes-using-alpine/)
- [_How to Write Dockerfiles for Python Web Apps_](https://blog.hasura.io/how-to-write-dockerfiles-for-python-web-apps-6d173842ae1d/)
- [_Building small python Docker images, How to?_](https://medium.com/bytes-mania/building-small-python-docker-images-how-to-8bb539d3e6fc)

---

## Lisensi

Lisensi yang dipergunakan adalah [MIT](LICENSE).
